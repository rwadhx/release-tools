# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::GraphqlAdapter do
  let(:token) { 'token' }
  let(:graphql_adapter) { described_class.new(token: token, endpoint: 'https://example.org') }

  describe '#query' do
    let(:client) { instance_double(GraphQL::Client, query: response) }
    let(:errors) { [] }
    let(:response_data) { nil }
    let(:response) do
      double(errors: errors, data: response_data)
    end

    subject(:query) { graphql_adapter.query(:graphql_query, source: 'gitlab-org/gitlab') }

    before do
      allow(graphql_adapter).to receive(:client).and_return(client)
    end

    it 'sends request through GraphQL::Client' do
      expect(client).to receive(:query).with(:graphql_query, variables: { source: 'gitlab-org/gitlab' }, context: { token: token })

      query
    end

    context 'when response has errors' do
      let(:exception_messages) { HashWithIndifferentAccess.new({ field: ['GraphQL Error'] }) }
      let(:errors) { double(blank?: false, messages: exception_messages) }

      it 'raises error' do
        expect(described_class.logger).to receive(:warn).with(described_class::GRAPHQL_ERROR_MESSAGE, query: :graphql_query, errors: exception_messages)

        expect { query }.to raise_error(described_class::GraphqlError, %|#{described_class::GRAPHQL_ERROR_MESSAGE}: {"field":["GraphQL Error"]}|)
      end
    end

    context 'when response is a simple hash' do
      let(:response_data) { { id: 1 } }

      it 'does not raise error' do
        expect { query }.not_to raise_error
      end

      it { is_expected.to eq(response_data) }
    end
  end

  describe '#dump_schema' do
    subject(:dump_schema) { graphql_adapter.dump_schema }

    it 'dumps the schema' do
      expect(GraphQL::Client)
        .to receive(:dump_schema)
        .with(instance_of(GraphQL::Client::HTTP), 'config/graphql_schema.json')

      dump_schema
    end
  end
end
