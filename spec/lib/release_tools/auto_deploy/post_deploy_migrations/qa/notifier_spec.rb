# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::Qa::Notifier do
  let(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  let(:fake_notification) do
    stub_const('ReleaseTools::Slack::QaNotification', spy)
  end

  subject(:notifier) do
    described_class.new(
      pipeline_id: '123',
      environment: 'gprd_cny'
    )
  end

  describe '#execute' do
    context "when qa pipeline can't be found" do
      it 'does nothing' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([])

        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .not_to receive(:new)

        notifier.execute
      end
    end

    context 'when qa pipelines can be found' do
      let(:gprd_cny_deployer_pipeline) do
        create(
          :pipeline,
          :success,
          web_url: 'https://test.gitlab.net/deployer/-/pipelines/123'
        )
      end

      let(:gprd_cny_bridge) do
        create(
          :gitlab_response,
          name: 'deploy:gprd_cny',
          status: 'success',
          downstream_pipeline: gprd_cny_deployer_pipeline
        )
      end

      let(:qa_pipeline) do
        create(
          :pipeline,
          :failed,
          web_url: 'https://test.gitlab.net/quality/-/pipelines/456'
        )
      end

      let(:qa_smoke_bridge) do
        create(
          :gitlab_response,
          name: 'qa:smoke:post_deploy_migrations:gstg',
          status: smoke_status,
          downstream_pipeline: qa_pipeline
        )
      end

      before do
        bridges = [gprd_cny_bridge, qa_smoke_bridge]

        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return(bridges)

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))
      end

      context 'with a smoke failure' do
        let(:smoke_status) { 'failed' }
        let(:smoke_main_status) { 'success' }

        it 'sends a slack notification' do
          expect(ReleaseTools::GitlabOpsClient)
            .to receive(:pipeline_bridges)
            .once

          expect(ReleaseTools::Slack::QaNotification)
            .to receive(:new)
            .with(
              deploy_version: nil,
              pipelines: [qa_smoke_bridge],
              environment: 'gprd_cny'
            )

          notifier.execute
        end
      end
    end
  end
end
