# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::MinimumCommit do
  describe '#sha' do
    subject(:commit) do
      described_class
        .new(ReleaseTools::Project::GitlabEe)
    end

    it 'returns the SHA for an auto-deploy branch' do
      tag = double(:tag, name: '13.11.202004131220')

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:tags)
        .with(
          described_class::TAGS_PROJECT,
          sort: 'desc',
          order_by: 'updated',
          per_page: 1
        )
        .and_return(Gitlab::PaginatedResponse.new([tag]))

      product_version = ReleaseTools::ProductVersion.new('13.11.202004131220')
      allow(product_version).to receive(:metadata).and_return(
        {
          'releases' => {
            'gitlab-ee' => { 'sha' => 'kittens' }
          }
        }
      )

      expect(ReleaseTools::ProductVersion)
        .to receive(:new)
        .with(ReleaseTools::Version.new('13.11.202004131220'))
        .and_return(product_version)

      expect(commit.sha).to eq('kittens')
    end

    it 'returns nil when no tags are found' do
      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:tags)
        .with(
          described_class::TAGS_PROJECT,
          sort: 'desc',
          order_by: 'updated',
          per_page: 1
        )
        .and_return(Gitlab::PaginatedResponse.new([]))

      expect(commit.sha).to be_nil
    end
  end
end
