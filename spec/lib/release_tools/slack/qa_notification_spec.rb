# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::QaNotification do
  let(:deploy_version) { '14.6.202112031820-3605bffdaf6.c516dd235a1' }
  let(:environment) { 'gprd-cny' }

  let(:qa_pipeline) do
    create(
      :pipeline,
      :failed,
      web_url: 'https://test.com/quality/-/pipelines/123'
    )
  end

  let(:qa_bridge) do
    create(
      :gitlab_response,
      name: qa_name,
      status: :failed,
      downstream_pipeline: qa_pipeline
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipelines: [qa_bridge],
      environment: environment
    )
  end

  describe '#execute' do
    let(:qa_name) { 'qa:smoke:gprd-cny' }

    let(:context_elements) do
      [
        { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' }
      ]
    end

    before do
      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return({})
    end

    context 'with a smoke pipeline' do
      it 'sends a slack message' do
        block_content =
          ":canary: :ci_failing: *QA gprd-cny* *smoke* <https://test.com/quality/-/pipelines/123|failed> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "qa gprd-cny failed 14.6.202112031820-3605bffdaf6.c516dd235a1",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'with a smoke-main pipeline' do
      let(:qa_name) { 'qa:smoke-main:gprd-cny' }

      it 'sends a slack message' do
        block_content =
          ":canary: :ci_failing: *QA gprd-cny* *smoke-main* <https://test.com/quality/-/pipelines/123|failed> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "qa gprd-cny failed 14.6.202112031820-3605bffdaf6.c516dd235a1",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'with two pipelines' do
      let(:qa_smoke_bridge) do
        create(
          :gitlab_response,
          name: 'qa:smoke:gprd-cny',
          status: :failed,
          downstream_pipeline: qa_pipeline
        )
      end

      let(:qa_smoke_main_bridge) do
        create(
          :gitlab_response,
          name: 'qa:smoke-main:gprd-cny',
          status: :failed,
          downstream_pipeline: qa_pipeline
        )
      end

      subject(:notifier) do
        described_class.new(
          deploy_version: deploy_version,
          pipelines: [qa_smoke_bridge, qa_smoke_main_bridge],
          environment: environment
        )
      end

      it 'sends a slack message' do
        block_content =
          ":canary: :ci_failing: *QA gprd-cny* <https://test.com/quality/-/pipelines/123|smoke> and <https://test.com/quality/-/pipelines/123|smoke-main> failed `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "qa gprd-cny failed 14.6.202112031820-3605bffdaf6.c516dd235a1",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'without deploy_version' do
      let(:deploy_version) { nil }

      it 'sends a slack message' do
        block_content =
          ":canary: :ci_failing: *QA gprd-cny* *smoke* <https://test.com/quality/-/pipelines/123|failed>"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "qa gprd-cny failed ",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'with dry-run' do
      it 'does nothing' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        notifier.execute
      end
    end
  end
end
