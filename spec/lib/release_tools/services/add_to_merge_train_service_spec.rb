# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::AddToMergeTrainService do
  let(:merge_request) { ReleaseTools::MergeRequest.new }
  let(:token) { 'a token' }
  let(:sha) { 'abc123' }
  let(:approve_service) { instance_double(ReleaseTools::Services::ApproveService) }

  subject(:service) { described_class.new(merge_request, token: token) }

  before do
    allow(ReleaseTools::Services::ApproveService).to receive(:new).and_return(approve_service)
  end

  describe '#execute' do
    it 'approves the merge request, and adds it to the Merge Train' do
      expect(service).to receive(:validate!)
      expect(approve_service).to receive(:execute)
      expect(service).to receive(:add_to_merge_train)

      without_dry_run do
        service.execute(sha)
      end
    end

    context 'when we have a connection problem' do
      it 'does retry' do
        # first run
        expect(approve_service).to receive(:execute).and_raise(gitlab_error(:BadRequest))

        # second run
        expect(service).to receive(:validate!).twice
        expect(approve_service).to receive(:execute)
        expect(service).to receive(:add_to_merge_train)

        without_dry_run do
          service.execute(sha)
        end
      end
    end
  end

  describe '#validate!' do
    before do
      allow(ReleaseTools::GitlabClient).to receive(:merge_trains_enabled?)
    end

    context 'when Merge Trains are not enabled for the project' do
      it 'raises an error' do
        allow(merge_request).to receive(:remote_issuable).and_return(nil)

        expect(ReleaseTools::GitlabClient)
          .to receive(:merge_trains_enabled?)
          .with(
            merge_request.project
          ).and_return(false)

        expect do
          service.validate!
        end.to raise_error(described_class::MergeTrainsNotEnabledError)
      end
    end
  end

  describe '#add_to_merge_train' do
    it 'delegates to Gitlab::Client#add_to_merge_train' do
      allow(merge_request).to receive(:iid).and_return(double(:iid))
      allow(merge_request).to receive(:project_id).and_return(double(:project_id))

      expect(ReleaseTools::GitlabClient)
        .to receive(:add_to_merge_train)
        .with(
          merge_request,
          sha,
          token: token
        )

      without_dry_run do
        service.add_to_merge_train(sha)
      end
    end
  end
end
