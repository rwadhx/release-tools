# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class Coordinator
      def initialize
        @versions = ReleaseTools::Versions.next_versions.map(&:previous_patch)
      end

      # Returns a list of pressure of the last three versions
      # Example:
      #
      # [
      #   { version: '15.5', pressure: [{'gitlab-org/gitlab': 1},{'gitlab-org/omnibus-gitlab': 1}]},
      #   { version: '15.4', pressure: [{'gitlab-org/gitlab': 2},{'gitlab-org/omnibus-gitlab': 2}]},
      #   { version: '15.3', pressure: [{'gitlab-org/gitlab': 3},{'gitlab-org/omnibus-gitlab': 3}]}
      # ]
      def pressure
        versions.map do |version|
          version_data = { version: version.to_minor, pressure: {} }

          projects.each do |project|
            version_data[:pressure][project.path] = UnreleasedCommits.new(version, project).total_pressure
          end

          version_data
        end
      end

      # Returns a list of merge requests and pressure of the last three versions
      # Example:
      #
      # [
      #   {
      #     version: '15.5.5',
      #     pressure: 2,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   },
      #   {
      #     version: '15.4.6',
      #     pressure: 2,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   },
      #   {
      #     version: '15.3.8',
      #     pressure: 3,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   }
      # ]
      def merge_requests
        versions.map do |version|
          version_data = { version: version.to_minor, pressure: 0, merge_requests: {} }

          projects.map do |project|
            merge_requests = UnreleasedMergeRequests.new(project, version).execute
            version_data[:merge_requests][project.to_s] = merge_requests
            version_data[:pressure] += merge_requests.count
          end

          version_data
        end
      end

      private

      attr_reader :versions

      def projects
        ReleaseTools::ManagedVersioning::PROJECTS
      end
    end
  end
end
