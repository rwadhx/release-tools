package labels

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBaseName(t *testing.T) {
	name := "a_label"

	l := Nil(name)

	require.Equal(t, name, l.Name(), "label name must match the constructor parameter")
}

func TestBaseValues(t *testing.T) {
	label := Nil("name")

	require.Nil(t, label.Values(), "Nil labels cannot provide a values list")
}

func TestBaseCheckValue(t *testing.T) {
	label := Nil("name")

	assert.False(t, label.CheckValue("foo"), "Nil labels cannot accept values")
	assert.False(t, label.CheckValue("bar"), "Nil labels cannot accept values")
	assert.False(t, label.CheckValue("baz"), "Nil labels cannot accept values")
}
