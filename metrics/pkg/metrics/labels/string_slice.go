package labels

func FromValues(name string, values []string) *stringSlice {
	return &stringSlice{
		base:   &base{name: name},
		values: values,
	}
}

type stringSlice struct {
	values []string

	*base
}

func (s *stringSlice) Values() []string {
	return s.values
}

func (s *stringSlice) CheckValue(value string) bool {
	for _, allowedValue := range s.values {
		if value == allowedValue {
			return true
		}
	}

	return false
}
