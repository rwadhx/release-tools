package main

import (
	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/handlers"
	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics/labels"
)

// initMetrics initializes and resets all the metrics.
// Every metrics that requires an API handler must be returned in []handlers.Pluggable
//
// NOTE: the labels order is important! Pay attention to never change metric.WithLabel
// order when initializing a metric.
func initMetrics() ([]handlers.Pluggable, error) {
	_, err := metrics.NewCounterVec(
		metrics.WithName("info"),
		metrics.WithSubsystem("version"),
		metrics.WithHelp("Version info metadata"),
		metrics.WithLabel(labels.Nil("build_date")),
		metrics.WithLabel(labels.Nil("revision")),
		metrics.WithLabelReset(BuildDate, Revision))
	if err != nil {
		return nil, err
	}

	pluggables := make([]handlers.Pluggable, 0)
	err = initDeploymentMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initPackagesMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	err = initAutoDeployMetrics(&pluggables)
	if err != nil {
		return pluggables, err
	}

	return pluggables, nil
}

func initDeploymentMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "deployment"
	deploymentLabelValues := map[string][]string{
		"deployment_type": {"coordinator_pipeline"},
		"status":          {"success", "failed"},
		"target_env":      {"gstg-ref", "gstg-cny", "gstg", "gprd-cny", "gprd"},
	}

	durationHistogram, err := metrics.NewHistogramVec(
		metrics.WithName("duration_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Duration of the coordinated deployment pipeline, from staging to production"),
		metrics.WithBuckets(prometheus.LinearBuckets(12_600, 30*60, 14)), // 14 buckets of 30 minutes ranging from 3.5hrs to 10h.
		metrics.WithLabel(labels.FromValues("deployment_type", deploymentLabelValues["deployment_type"])),
		metrics.WithLabel(labels.FromValues("status", deploymentLabelValues["status"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewHistogram(durationHistogram))

	durationGauge, err := metrics.NewGaugeVec(
		metrics.WithName("duration_last_seconds"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Duration of the last coordinated deployment pipeline, from staging to production"),
		metrics.WithLabel(labels.FromValues("deployment_type", deploymentLabelValues["deployment_type"])),
		metrics.WithLabel(labels.FromValues("status", deploymentLabelValues["status"])),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(durationGauge))

	deploymentStartedCounter, err := metrics.NewCounterVec(
		metrics.WithName("started_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments started for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(deploymentStartedCounter))

	deploymentCanRollbackCounter, err := metrics.NewCounterVec(
		metrics.WithName("can_rollback_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of deployments suitable for rollback for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(deploymentCanRollbackCounter))

	rollbacksCounter, err := metrics.NewCounterVec(
		metrics.WithName("rollbacks_started_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of rollbacks started for each environment"),
		metrics.WithLabel(labels.FromValues("target_env", deploymentLabelValues["target_env"])),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(rollbacksCounter))

	return nil
}

func initPackagesMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "packages"

	//NOTE (nolith): we should consider tracking also RCs and public packages
	pkgTypes := []string{"auto_deploy"}

	taggingCounter, err := metrics.NewCounterVec(
		metrics.WithName("tagging_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of tagged packages by package type"),
		metrics.WithLabel(labels.FromValues("pkg_type", pkgTypes)),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewCounter(taggingCounter))

	return nil
}

func initAutoDeployMetrics(pluggables *[]handlers.Pluggable) error {
	subsystem := "auto_deploy"

	roles := []string{"gstg-cny", "gprd-cny", "gstg", "gprd"}

	pressureGauge, err := metrics.NewGaugeVec(
		metrics.WithName("pressure"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of commits not yet deployed to an environment"),
		metrics.WithLabel(labels.FromValues("role", roles)),
		metrics.WithCartesianProductLabelReset(),
	)
	if err != nil {
		return err
	}

	*pluggables = append(*pluggables, handlers.NewGauge(pressureGauge))

	return nil
}
